
console.log("Bonjour")
const express = require('express');
const mongoose = require('mongoose');
const app = express()


//ecoute sur le port 3000
app.listen(3000, function(){
  console.log("Example app listening on port 3000");
});

//GET printing hello word
app.get('/', function (req, res){
  res.send("Hello word ! ");
});

//url de connexion à la BDD
const DATABASE_CONNECTION = 'mongodb://mongo/Sncf';

//Instanciation du modele
var declarationSchema = mongoose.Schema({
  Annee : Number,
  Mois : Number,
  Date : String,
  Gare : String,
  Nature : String,
  Type : String
});

//Initialize Mongo
mongoose.connect(DATABASE_CONNECTION);
console.log('Trying to connect to' - DATABASE_CONNECTION);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: we might not be as connected as I thought'))
db.once('open', function(){
    console.log('We are connected you and I');
})

// GET declarations from annee
app.get('/declarations/:annee', function(req, res){
  Declaration = mongoose.model('Declaration', declarationSchema,"declaration_"+req.params.annee);
  Declaration.find(function(err, declarations){
    if (err) return res.error(err);
    //console.log(declarations);
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.json(declarations);
  });
});