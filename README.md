# Projet Docker 
Le but de ce projet est de déployer le projet de visualisation avec Docker 
## Author  
Demont ZHANG  
Jiaqian DU  
Yves OBAME EDOU

## Liste des containers 

ASSUREZ VOUS DE LIBERER LES PORTS SUIVANTS: 27017, 8081, 80, 3000

*  **mongo**

    **Rôle :** Contient la base de données 

    **Volume :** ./data où sont stocké les données.

    **Adresse :** localhost:27017

![mongoDB](readme_images/mongoDB.png)

*  **mongo-express**

    **Rôle :** Permet d'accéder à la base avec une interface graphique. 

    **Adresse :** localhost:8081

    Les données sont stockées dans la base __Sncf__ subivisée en collections par année.

![mongoExpress](readme_images/mongoExpress.png)

![mongoExpress2](readme_images/mongoExpress2.png)

![mongoExpress3](readme_images/mongoExpress3.png)

* **web** 

    **Rôle :** API NodeJS permettant de requêter la base MongoDB

    **Volume :** ./web-site contenant le Dockerfile permettant de configurer l'image et les fichiers de traitement des données.

    **Adresse :** localhost:3000

    **Accès au données :** localhost:3000/declarations/*année*  
    (Remplacer *année* par un nombre variant de 2013 à 2018)

![api](readme_images/api.png)

![api2](readme_images/api2.png)

* **web-server**

    **Rôle :** Server herbergeant l'application.

    **Volume :** ./www contenant les pages de l'application.

    **Adresse :** localhost:80/index.html (Il faut s'assurer que le port 80 est libéré)

    **Accès à l'application :** clic sur bouton *ACCEDER AUX GRAPHES* pour être redirigé sur morris.html(la page contenant les graphes)

![web2](readme_images/web2.png)

![web3](readme_images/web3.png)
